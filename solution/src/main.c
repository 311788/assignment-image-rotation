#include "../include/file.h"
#include "../include/bmp.h"
#include "../include/rotate.h"
#include <malloc.h>

//void err_finish(FILE** f1, FILE** f2){
//    free(image);
//    file_close(f1);
//    file_close(f2);
//}
int main(int argc, char **argv) {
    (void) argc;
    (void) argv;
    if (argc != 3) {
        fprintf(stderr, "Wrong number of input arguments\n");
        return 1;
    }
    static FILE *f1;
    static FILE *f2;
    if (file_open(&f1, argv[1], "rb") != OPEN_OK) {
        fprintf(stderr, "Not possible open file to read");
        return 1;
    }
    if (file_open(&f2, argv[2], "wb") != OPEN_OK) {
        fprintf(stderr, "Not possible open file to write");
        return 1;
    }
    struct image image;
    enum read_status err_from = from_bmp(f1, &image);
    if (err_from != READ_OK) {
        fprintf(stderr, "Problem with format while reading %d\n", err_from);
    }
    struct image routate_image = rotate(image);
    if (to_bmp(f2, &routate_image) != WRITE_OK) {
        fprintf(stderr, "Problem with format while writing");
    }
    img_delete(image);
    img_delete(routate_image);
    file_close(&f1);
    file_close(&f2);
    fprintf(stderr, "Finish successfully");
    return 0;
}
