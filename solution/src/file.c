#include "../include/file.h"


enum open_status file_open(FILE **const f, const char *name, const char *mode) {
    if (!(*f = fopen(name, mode))) {
        return OPEN_ERROR;
    }
    return OPEN_OK;
}

enum close_status file_close(FILE **const file) {
    if (!fclose(*file)) {
        return CLOSE_ERROR;
    }
    return CLOSE_OK;
}

