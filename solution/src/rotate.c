#include "../include/image.h"
#include <malloc.h>

struct image rotate(struct image const source) {
    struct pixel *pixels = malloc(sizeof(struct pixel) * source.height * source.width);
    size_t k = source.height * source.width - 1;
    for (uint64_t i = 0; i < source.width; i++) {
        for (uint64_t j = 0; j < source.height; j++) {
            pixels[k] = source.data[j * source.width + source.width - 1 - i];
            k--;
        }
    }
    return img_create(source.height, source.width, pixels);
}
