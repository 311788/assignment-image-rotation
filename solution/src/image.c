#include "../include/image.h"
#include <malloc.h>

struct image img_create(uint64_t width, uint64_t height, struct pixel *data) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = data
    };
}

void img_delete(struct image img) {
    free(img.data);
}

