#include "../include/bmp.h"
#include <malloc.h>
#include <stdbool.h>

#define BF_TYPE 19778
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_COUNT 24
#define BIX_PELS_PER_METER 2834
#define BIY_PELS_PER_METER 2834

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint8_t bmp_padding(const size_t width) {
    return width % 4;
}

static bool bmp_header_valid(struct bmp_header const *bh) {
    return bh->biBitCount == 24;
}

/*  deserializer   */
enum read_status from_bmp(FILE *const in, struct image *const img) {
    struct bmp_header bh = {0};
    if (!fread(&bh, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_SIGNATURE;
    }
    if (!bmp_header_valid(&bh)) return READ_INVALID_HEADER;
    struct pixel *pixels = malloc(sizeof(struct pixel) * bh.biHeight * bh.biWidth);
    for (uint32_t i = 0; i < bh.biHeight; i++) {
        if (!fread(pixels + i * bh.biWidth, bh.biWidth * sizeof(struct pixel), 1, in)) {
            free(pixels);
            return READ_INVALID_BITS;
        }
        if (fseek(in, bmp_padding(bh.biWidth), SEEK_CUR)) {
            free(pixels);
            return READ_INVALID_BITS;
        }
    }
    *img = img_create(bh.biWidth, bh.biHeight, pixels);
    return READ_OK;
}

static uint32_t image_bmp_image_size(struct image const *img) {
    return img->height * img->width * sizeof(struct pixel) + (img->height) * bmp_padding(img->width);
}

static uint32_t image_bmp_file_size(struct image const *img) {
    return image_bmp_image_size(img) + sizeof(struct bmp_header);
}

static struct bmp_header bmp_header_fill(struct image const *img) {
    return (struct bmp_header) {
            .bfType = BF_TYPE,
            .bfileSize= image_bmp_file_size(img),
            .bOffBits=sizeof(struct bmp_header),
            .biSize =BI_SIZE,
            .biWidth= img->width,
            .biHeight= img->height,
            .biPlanes=BI_PLANES,
            .biBitCount=BI_COUNT,
            .biSizeImage=image_bmp_image_size(img),
            .biXPelsPerMeter=BIX_PELS_PER_METER,
            .biYPelsPerMeter=BIY_PELS_PER_METER,
    };
}

enum write_status to_bmp(FILE *const out, struct image const *img) {
    struct bmp_header bh = bmp_header_fill(img);
    if (!fwrite(&bh, sizeof(struct bmp_header), 1, out)) {
        free(img->data);
        return WRITE_ERROR;
    }
    for (uint64_t i = 0; i < img->height; ++i) {
        if (!fwrite(img->data + i * img->width, img->width * sizeof(struct pixel), 1, out)) {
            return WRITE_ERROR;
        }
//        printf("%" PRIu8 "\n", bmp_padding(img->width));
        {
            char padding[4] = {0};
            if (!fwrite(padding, bmp_padding(img->width), 1, out)) {
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}
