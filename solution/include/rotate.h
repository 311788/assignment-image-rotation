#ifndef LAB3_ROTATE_H
#define LAB3_ROTATE_H

struct image rotate(struct image const source);

#endif //LAB3_ROTATE_H
