#ifndef LAB3_FILE_H
#define LAB3_FILE_H

#include <stdio.h>

/*  serializer   */
enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR
    /* коды других ошибок  */
};

enum open_status file_open(FILE **const file, const char *name, const char *mode);

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
    /* коды других ошибок  */
};

enum close_status file_close(FILE **const file);

#endif //LAB3_FILE_H
