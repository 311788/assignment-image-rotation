#ifndef LAB3_IMAGE_H
#define LAB3_IMAGE_H

#include <stdint.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image img_create(uint64_t width, uint64_t height, struct pixel* data);

void img_delete(struct image img);

#endif //LAB3_IMAGE_H
